package com.example.root.wifiscanner;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.MovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;



public class Client extends AppCompatActivity {
    TextView mainText , side;
    WifiManager mainWifi;
    WifiReceiver receiverWifi;
    List<ScanResult> wifiList;
    StringBuilder sb = new StringBuilder();
    static Map<Integer,Character> map=new HashMap<Integer,Character>();
    Button b ;
    private static final int PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION = 1001;
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        /// Map table
        map.put(0,'a'); map.put(1,'b');map.put(2,'c');map.put(3,'d');map.put(4,'e');map.put(5,'f');map.put(6,'g');map.put(7,'h');map.put(8,'i');map.put(9,'j');map.put(10,'k');map.put(11,'l');map.put(12,'m');map.put(13,'n');map.put(14,'o');map.put(15,'p');map.put(16,'q');map.put(17,'r');map.put(18,'s');map.put(19,'t');map.put(20,'u');map.put(21,'v');map.put(22,'w');map.put(23,'x');map.put(24,'y');map.put(25,'z');
        map.put(26,'A');map.put(27,'B');map.put(28,'C');map.put(29,'D');map.put(30,'E');map.put(31,'F');map.put(32,'G');map.put(33,'H');map.put(34,'I');map.put(35,'J');map.put(36,'K');map.put(37,'L');map.put(38,'M');map.put(39,'N');map.put(40,'O');map.put(41,'P');map.put(42,'Q');map.put(43,'R');map.put(44,'S');map.put(45,'T');map.put(46,'U');map.put(47,'V');map.put(48,'W');map.put(49,'X');map.put(50,'Y');map.put(51,'Z');
        map.put(52,'0');map.put(53,'1');map.put(54,'2');map.put(55,'3');map.put(56,'4');map.put(57,'5');map.put(58,'6');map.put(59,'7');map.put(60,'8');map.put(61,'9');

        mainText = (TextView) findViewById(R.id.mainText);
        mainText.setMovementMethod(new ScrollingMovementMethod());
        side = findViewById(R.id.side) ;
        // addContentView(mainText,new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));


        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION);
        }
        // Initiate wifi service manager
        mainWifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        // Check for wifi is disabled
        if (mainWifi.isWifiEnabled() == false)
        {
            // If wifi disabled then enable it
            Toast.makeText(getApplicationContext(), "wifi is disabled..making it enabled",
                    Toast.LENGTH_LONG).show();

            mainWifi.setWifiEnabled(true);
        }

        // wifi scaned value broadcast receiver
        receiverWifi = new WifiReceiver();

        // Register broadcast receiver
        // Broacast receiver will automatically call when number of wifi connections changed
        registerReceiver(receiverWifi, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        mainWifi.startScan();
        mainText.setText("Starting Scan...");
        side.setText("");


        b = findViewById(R.id.buttscan) ;
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                side.setText("");
                mainText.setText("Starting Scan...");
                mainWifi.startScan() ;
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, "Refresh");
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mainWifi.startScan();
        mainText.setText("Starting Scan");
        side.setText("");
        // do Your Work Here
        return super.onOptionsItemSelected(item);
    }

    protected void onPause() {
        unregisterReceiver(receiverWifi);
        super.onPause();
    }

    protected void onResume() {
        registerReceiver(receiverWifi, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        super.onResume();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText( getApplicationContext() , "Location ok" , Toast.LENGTH_SHORT).show();

            // TODO: What you want to do when it works or maybe .PERMISSION_DENIED if it works better
        }
    }

    // Broadcast receiver class called its receive method
    // when number of wifi connections changed

    class WifiReceiver extends BroadcastReceiver {

        // This method call when number of wifi connections changed
        public void onReceive(Context c, Intent intent) {

            sb = new StringBuilder();
            boolean found = false ;
            wifiList = mainWifi.getScanResults();
            side.setText("\n        Number Of Wifi connections :"+wifiList.size()+"\n\n");
            String Decrypted = "" ;
            for(int i = 0; i < wifiList.size(); i++){

                sb.append("        " + new Integer(i+1).toString() + ". ");
                sb.append( wifiList.get(i).SSID + " - " + wifiList.get(i).BSSID);
                sb.append("\n\n");

                if(wifiList.get(i).SSID.startsWith("SAFE_QUIZ:"))
                {   String key = "1561651184751234";
                    String encrypted =  wifiList.get(i).SSID.substring(10) ;
                    Decrypted = decrypt(encrypted , key) ;
                    mainText.append("Got key as " + Decrypted + "\n");
                    found = true ;
                }
            }
            if(! found)
            {
                mainText.append("No key exchanged\n");
            }

            mainText.append(sb);
        }

    }
    /// for encryption
    private static byte[] parseHexBinary(String s)
            throws IllegalArgumentException {
        if (s == null) {
            return new byte[0];
        }
        s = s.trim();
        int length = s.length();

        if (length % 2 != 0) {
            throw new IllegalArgumentException("Invalid hex string length.");
        }

        byte[] result = new byte[length / 2];
        for (int i = 0; i < length; i += 2) {
            result[i/2] = (byte) Integer.parseInt(s.substring(i, i + 2), 16);
        }
        return result;
    }
    private static byte[] encrypt(byte[] raw, byte[] clear) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        byte[] encrypted = cipher.doFinal(clear);
        return encrypted;
    }

    private static byte[] decrypt(byte[] raw, byte[] encrypted) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        byte[] decrypted = cipher.doFinal(encrypted);
        return decrypted;
    }
    public static String encrypt(String text, final String key) {
        String res = "";
        for (int i = 0, j = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9')){
                int no = (getKey(c) + getKey(key.charAt(j))) % 62;
                res += map.get(no);
                j = ++j % key.length();
            }
            else
                res += c;
        }
        return res;
    }

    public static String decrypt(String text, final String key) {
        String res = "";
        for (int i = 0, j = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9')){
                int no = (getKey(c) - getKey(key.charAt(j)) + 62) % 62;
                res += map.get(no);
                j = ++j % key.length();
            }
            else
                res += c;
        }
        return res;
    }

    static int getKey(char c){
        Set set=map.entrySet();//Converting to Set so that we can traverse
        Iterator itr=set.iterator();
        int ans = 0;
        while(itr.hasNext()){
            //Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry=(Map.Entry)itr.next();
            if((char)entry.getValue() == c)
                ans = (int)entry.getKey();
        }

        return ans;
    }

}
