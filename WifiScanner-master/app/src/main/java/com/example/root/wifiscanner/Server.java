package com.example.root.wifiscanner;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.util.*;
import java.lang.*;

public class Server extends AppCompatActivity {
    TextView mainText , side;
    WifiManager mainWifi;
    List<ScanResult> wifiList;
    StringBuilder sb = new StringBuilder();
    Button b ;
    static Map<Integer,Character> map=new HashMap<Integer,Character>();
    private static final int PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION = 1001;
    @Override
    public void onCreate(Bundle savedInstanceState)  {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main2);


        mainText = (TextView) findViewById(R.id.mainText);
        mainText.setMovementMethod(new ScrollingMovementMethod());
        side = findViewById(R.id.side) ;
        // addContentView(mainText,new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));


        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION);


        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Settings.System.canWrite(this.getApplicationContext())) {

            } else {
                Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS);
                intent.setData(Uri.parse("package:" + this.getPackageName()));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }
        // Initiate wifi service manager
        mainWifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        b = findViewById(R.id.buttscan) ;
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isApOn()) {
                    turnWifiApOff();
                    startAnnounce();
                }

                        startAnnounce();



            }
        });

        map.put(0,'a'); map.put(1,'b');map.put(2,'c');map.put(3,'d');map.put(4,'e');map.put(5,'f');map.put(6,'g');map.put(7,'h');map.put(8,'i');map.put(9,'j');map.put(10,'k');map.put(11,'l');map.put(12,'m');map.put(13,'n');map.put(14,'o');map.put(15,'p');map.put(16,'q');map.put(17,'r');map.put(18,'s');map.put(19,'t');map.put(20,'u');map.put(21,'v');map.put(22,'w');map.put(23,'x');map.put(24,'y');map.put(25,'z');
        map.put(26,'A');map.put(27,'B');map.put(28,'C');map.put(29,'D');map.put(30,'E');map.put(31,'F');map.put(32,'G');map.put(33,'H');map.put(34,'I');map.put(35,'J');map.put(36,'K');map.put(37,'L');map.put(38,'M');map.put(39,'N');map.put(40,'O');map.put(41,'P');map.put(42,'Q');map.put(43,'R');map.put(44,'S');map.put(45,'T');map.put(46,'U');map.put(47,'V');map.put(48,'W');map.put(49,'X');map.put(50,'Y');map.put(51,'Z');
        map.put(52,'0');map.put(53,'1');map.put(54,'2');map.put(55,'3');map.put(56,'4');map.put(57,'5');map.put(58,'6');map.put(59,'7');map.put(60,'8');map.put(61,'9');

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText( getApplicationContext() , "Location ok" , Toast.LENGTH_SHORT).show();

            // TODO: What you want to do when it works or maybe .PERMISSION_DENIED if it works better
        }
    }

    // Broadcast receiver class called its receive method
    // when number of wifi connections changed

    // hotspot
    public boolean isApOn() {
        try {
            Method method = mainWifi.getClass().getDeclaredMethod("isWifiApEnabled");
            method.setAccessible(true);
            return (Boolean) method.invoke(mainWifi);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    public boolean turnWifiApOff() {
        WifiConfiguration wificonfiguration = null;
        try {
            Method method = mainWifi.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class);
            method.invoke(mainWifi, null, false);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    public boolean createNewNetwork(String ssid, String password) {
       mainWifi.setWifiEnabled(false); // turn off Wifi

// creating new wifi configuration
        WifiConfiguration myConfig = new WifiConfiguration();
        myConfig.SSID = ssid; // SSID name of netwok
        myConfig.preSharedKey = "Solutionis"; // password for network
        myConfig.allowedKeyManagement.set(4); // 4 is for KeyMgmt.WPA2_PSK which is not exposed by android KeyMgmt class
        myConfig.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN); // Set Auth Algorithms to open
        try {
            Method method = mainWifi.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class);
            return (Boolean) method.invoke(mainWifi, myConfig, true);  // setting and turing on android wifiap with new configrations
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;

    }
    /// for encryption
    private  byte[] parseHexBinary(String s)
            throws IllegalArgumentException {
        if (s == null) {
            return new byte[0];
        }
        s = s.trim();
        int length = s.length();
        mainText.setText("what the hell" + length+ "\n");
        if (length % 2 != 0) {
            throw new IllegalArgumentException("Invalid hex string length.");
        }

        byte[] result = new byte[length / 2];
        for (int i = 0; i < length; i += 2) {
            result[i/2] = (byte) Integer.parseInt(s.substring(i, i + 2), 16);
        }
        return result;
    }


    public static String encrypt(String text, final String key) {
        String res = "";
        for (int i = 0, j = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9')){
                int no = (getKey(c) + getKey(key.charAt(j))) % 62;
                res += map.get(no);
                j = ++j % key.length();
            }
            else
                res += c;
        }
        return res;
    }

    public static String decrypt(String text, final String key) {
        String res = "";
        for (int i = 0, j = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9')){
                int no = (getKey(c) - getKey(key.charAt(j)) + 62) % 62;
                res += map.get(no);
                j = ++j % key.length();
            }
            else
                res += c;
        }
        return res;
    }

    static int getKey(char c){
        Set set=map.entrySet();//Converting to Set so that we can traverse
        Iterator itr=set.iterator();
        int ans = 0;
        while(itr.hasNext()){
            //Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry=(Map.Entry)itr.next();
            if((char)entry.getValue() == c)
                ans = (int)entry.getKey();
        }

        return ans;
    }

    public void startAnnounce() {


        mainWifi.setWifiEnabled(false); // turn off Wifi

        //  mainText.append("\n " + isApOn() + "\n");
        EditText edit = findViewById(R.id.plain);
        String key_data = edit.getText().toString();
        if (!(key_data.matches("[0-9A-Za-z]*") && key_data.length() > 5 && key_data.length() < 11)) {
            edit.setError("Not a good Key");
            return;
        }

        String key = "1561651184751234";
        String plain = key_data.toString();


        String encrypted = "";
        String decrypted = "";
        try {
            encrypted = encrypt(plain, key);
            decrypted = decrypt(encrypted, key);
            mainText.append("Encrypted data :" + new String(encrypted) + " \n");
            mainText.append("Decrypted data :" + new String(decrypted) + " \n");
            createNewNetwork("SAFE_QUIZ:" + encrypted, "123");

        } catch (Exception e) {
            e.printStackTrace();
        }


    }



    }

